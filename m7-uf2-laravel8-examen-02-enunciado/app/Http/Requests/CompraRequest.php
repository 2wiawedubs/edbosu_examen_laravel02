<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class CompraRequest extends FormRequest
{
    protected $redirectRoute = 'home'; //ruta definida en alguno de los archivos de la carpeta routes
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "id" => "required",
            "quantity" => "min:0",
        ];
    }

    public function messages()
    {
        return [
            "id.required" => "Id del producto",
            "quantity" => "Cantidad del producto",
        ];
    }
    public function attributes()
    {
        return [
            "id" => "Id del producto",
            "quantity" => "Cantidad del producto",
            "action" => "Comprar o alquilar"
        ];
    }
    /**
     *  AJAX Response 
     */
    public function response(array $errors)
    {
        if ($this->expectsJson()) {
            return new JsonResponse($errors, 422);
        }
        return $this->redirector->to($this->getRedirectUrl())
            ->withInput($this->except($this->dontFlash))
            ->withErrors($errors, $this->errorBag);
    }
}
