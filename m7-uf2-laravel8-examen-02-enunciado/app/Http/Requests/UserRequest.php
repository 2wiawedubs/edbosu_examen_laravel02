<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          "name" => 'required',
          "email" => 'required',
          "address" => 'required',
          "password" => 'required',
          "password_confirmation" => 'required',
          "password_confirmation" => 'required',
        ];
    }   
    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
   public function messages()
   {
       return [
        "name.required" => "Falta el campo 'name'",
        "email.required" => "Falta el campo 'email'",
        "address.required" => "Falta el campo 'address'",
        "password.required" => "Falta el campo 'password'",
        "password_confirmation.required" => "Falta el campo 'password_confirmation'",
        "password_confirmation.required" => "Falta el campo 'password_confirmation'",
          
       ];
   }
}
